package com.google.bitcoin.core.merkle;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author Steve Coughlan
 *
 */
public class StringMerkleTree extends AbstractMerkleTree<String, StringMerkleBranch> {

	public static void main(String[] args) {

		List<String> list = Arrays.asList(new String[] { "1", "2", "3", "4", "5", "6" });

		StringMerkleTree tree = new StringMerkleTree(list);
		System.out.println(tree.toStringTree() + "\n");
		System.out.println(tree.toString() + "\n");

		int branchIndex = 3;

		for (int i = 0; i < list.size(); i++) {
			AbstractMerkleBranch branch = tree.getBranch(i);
			System.out.println(branch.toString());
			System.out.println(branch.validate(tree.getNode(i), tree.getRoot()));
			branch.validate();
		}

		tree.setFirstNode("*");
		tree.addNode("x");
		tree.addNode("y");
		tree.addNode("z");
		tree.addNode("Q");
		System.out.println(tree.toStringTree() + "\n");
	}

	public StringMerkleTree(List<String> elements) {
		super(elements);
	}

	StringMerkleTree() {
		super();
	}

	@Override
	public String makeParent(String left, String right) {
		return "(" + left + right + ")";
		// return "(.)(.)";
	}
	
	/* (non-Javadoc)
	 * @see com.google.bitcoin.core.merkle.AbstractMerkleTree#newBranch(int, java.lang.Object, java.lang.Object, java.util.List)
	 */
	@Override
	protected StringMerkleBranch newBranch(int index, String node, String root, List<String> branch) {
		return new StringMerkleBranch(index, node, root, branch);
	}

}
