package com.google.bitcoin.core.merkle;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author Steve Coughlan
 *
 * @param <D>
 */
public abstract class AbstractMerkleBranch<D> implements Serializable {

	//AbstractMerkleTree<D, ?> tree;

	/**
	 * index of the node in the original tree
	 */
	int nodeIndex;
	
	D node;
	
	D root;
	
	List<D> branches;

	public <B extends AbstractMerkleBranch<D>> AbstractMerkleBranch(AbstractMerkleTree<D, B> tree, int nodeIndex,
			List<D> branches) {
		super();
		this.node = tree.getNode(nodeIndex);
		this.root = tree.getRoot();
		this.nodeIndex = nodeIndex;
		this.branches = branches;
	}
	
	public <B extends AbstractMerkleBranch<D>> AbstractMerkleBranch(int nodeIndex,	List<D> branches) {
		super();
		this.nodeIndex = nodeIndex;
		this.branches = branches;
	}
	
	public <B extends AbstractMerkleBranch<D>> AbstractMerkleBranch(int nodeIndex, D node, D root, List<D> branches) {
		super();
		this.node = node;
		this.nodeIndex = nodeIndex;
		this.root = root;
		this.branches = branches;
	}

	/**
	 * Validate that this branch solves for the given merkle root
	 * 
	 * @param node
	 *            the node to check, this is required as the Satoshi brand of
	 *            merkle branch does not include the actual node.
	 * @param root
	 *            The merkleroot to validate against
	 */
	public boolean validate(D node, D root) {
		// for each level the branch only provides one of the nodes. We must
		// determine the order of the nodes
		// and calculate the other using the children from the previous
		// iteration.

		D parent = node;
		int levelIndex = nodeIndex;
		for (D branch : branches) {
			D left;
			D right;
			if (levelIndex % 2 == 0) {
				left = parent;
				right = branch;
			} else {
				right = parent;
				left = branch;
			}
			parent = makeParent(left, right);
			levelIndex /= 2;
		}
		return parent.equals(root);
	}
	
	/**
	 * Validate that this branch solves for the given merkle root
	 * @return true if solved
	 * @throws IllegalStateException if the branch was constructed without providing a non-null node and merkle root.
	 */
	public boolean validate() throws IllegalStateException {
		try {
			return validate(node, root);
		} catch (NullPointerException e) {
			throw new IllegalStateException("No merkle root or node has been provided.  Cannot validate a merkle branch with both.");
		}
	}

	protected abstract D makeParent(D left, D right);

	/**
	 * @return index of the node in the original merkle tree.
	 */
	public int getNodeIndex() {
		return nodeIndex;
	}

	/**
	 * @return the node for whom this branch validates.
	 */
	public D getNode() {
		return node;
	}

	/**
	 * @return the root of the parent merkle tree.
	 */
	public D getRoot() {
		return root;
	}

	/**
	 * @return the branches
	 */
	public List<D> getBranches() {
		return branches;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("index: ").append(nodeIndex);
		sb.append(" node: ").append(node);
		sb.append(" branch: ").append(branches);
		sb.append(" root: ").append(root);
		return sb.toString();
	}

}
