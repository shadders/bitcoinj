package com.google.bitcoin.core.merkle;

import static com.google.bitcoin.core.Utils.doubleDigestTwoBuffers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.bitcoin.core.Transaction;
import com.google.bitcoin.core.Utils;

/**
 * 
 * @author Steve Coughlan
 *
 */
public class TestTree {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		for (int index = 0; index < 100; index++) {
			int level = ((int) Math.ceil((Math.log(index)/Math.log(2.0)))) + 1;
			System.out.println("count: " + index + " levels: " + level);
		}
		
		int treeSize = 5;
		System.out.println("indexOf: " + AbstractMerkleTree.startIndexOf(1, treeSize));
		System.out.println("indexOf: " + AbstractMerkleTree.startIndexOf(2, treeSize));
		System.out.println("indexOf: " + AbstractMerkleTree.startIndexOf(3, treeSize));
		System.out.println("indexOf: " + AbstractMerkleTree.startIndexOf(4, treeSize));
		System.out.println("indexOf: " + AbstractMerkleTree.startIndexOf(5, treeSize));
		
		for (int index = 0; index < 12; index++) {
			int level = ((int) Math.ceil((Math.log(index)/Math.log(2.0)))) + 1;
			System.out.println("index: " + index + " level: " + AbstractMerkleTree.levelOf(index, 5));
		}
		
		for (int index = 0; index < 12; index++) {
			int level = ((int) Math.ceil((Math.log(index)/Math.log(2.0)))) + 1;
			System.out.println("index: " + index + " parent: " + AbstractMerkleTree.indexOfParent(index, 5));
		}
		
		List<String> nodes = Arrays.asList(new String[] {"1","2","3","4","5"});
		TestTree t = new TestTree();
		
		List<String> tree = t.buildMerkleTree(nodes);
		
		System.out.println(nodes);
		System.out.println(tree);
		
		List<String> branch = t.getMerkleBranch(tree, nodes, 3);
		System.out.println(branch);
		
		for (int index = 0; index < 100; index++) {
			if ((index % 2 == 0 ? index + 1 : index -1) != (index^1))
				System.out.println("bad XOR match for index: " + index);
			if ((index) / 2 != (index >> 1))
				System.out.println("bad rightshift match for index: " + index + " shifted: " + (index >> 1));
			
		}
	}
	
	private List<String> getMerkleBranch(List<String> tree, List<String> nodes, int index) {
		ArrayList<String> branch = new ArrayList<String>();
		 int j = 0;
	        for (int nSize = nodes.size(); nSize > 1; nSize = (nSize + 1) / 2)
	        {
	            //index^1 flips the last bit.  So if index is odd it becomes index -1
	        	//if index is even it becomes index + 1.  This gives the index of the
	        	//node's matching pair.
	        	//equivilent code would be:
	        	// index % 2 == 0 ? index + 1 : index -1
	        	int i = Math.min(index^1, nSize-1);
	            branch.add(tree.get(j+i));
	            
	            //right bitshift is equivilent to (index) / 2 (integer op so remainder is dropped)
	            index >>= 1;
	            j += nSize;
	        }
		return branch;
	}
	
	ArrayList<String> tree = new ArrayList();
	
	private List<String> buildMerkleTree(List<String> nodes) {
		ArrayList<String> tree = new ArrayList<String>();
		// Start by adding all the hashes of the transactions as leaves of the
		// tree.
		for (String t : nodes) {
			tree.add(t);
		}
		int levelOffset = 0; // Offset in the list where the currently processed
								// level starts.
		// Step through each level, stopping when we reach the root (levelSize
		// == 1).
		for (int levelSize = nodes.size(); levelSize > 1; levelSize = (levelSize + 1) / 2) {
			// For each pair of nodes on that level:
			for (int left = 0; left < levelSize; left += 2) {
				// The right hand node can be the same as the left hand, in the
				// case where we don't have enough
				// transactions.
				int rightIndex = Math.min(left + 1, levelSize - 1);
				String lefts = tree.get(levelOffset + left);
				String rights = tree.get(levelOffset + rightIndex);
				tree.add(addNodes(lefts, rights));
			}
			// Move to the next level.
			levelOffset += levelSize;
		}
		return tree;
	}
	
	
	String addNodes(String left, String right) {
		return "(" + left + right + ")";
	}

}
