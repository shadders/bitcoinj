package com.google.bitcoin.core.merkle;

import com.google.bitcoin.core.Sha256Hash;
import com.google.bitcoin.core.Utils;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Steve Coughlan
 *
 */
public class MerkleTree extends AbstractMerkleTree<Sha256Hash, MerkleBranch> implements Serializable {

	public MerkleTree() {
		super();
	}

	public MerkleTree(List<Sha256Hash> elements) {
		super(elements);
	}

	@Override
	public Sha256Hash makeParent(Sha256Hash left, Sha256Hash right) {
		byte[] leftBytes = Utils.reverseBytes(left.getBytes());
		byte[] rightBytes = Utils.reverseBytes(right.getBytes());
		return new Sha256Hash(Utils.reverseBytes(Utils.doubleDigestTwoBuffers(leftBytes, 0, 32, rightBytes, 0, 32)));
		
	}

	/* (non-Javadoc)
	 * @see com.google.bitcoin.core.merkle.AbstractMerkleTree#newBranch(int, java.lang.Object, java.lang.Object, java.util.List)
	 */
	@Override
	protected MerkleBranch newBranch(int index, Sha256Hash node, Sha256Hash root, List<Sha256Hash> branch) {
		return new MerkleBranch(index, node, root, branch);
	}

	/* (non-Javadoc)
	 * @see com.google.bitcoin.core.merkle.AbstractMerkleTree#getBranch(int)
	 */
	@Override
	public MerkleBranch getBranch(int index) {
		// TODO Auto-generated method stub
		return (MerkleBranch) super.getBranch(index);
	}
	
	

}
