package com.google.bitcoin.core.merkle;

import java.util.List;

/**
 * 
 * @author Steve Coughlan
 *
 */
public class StringMerkleBranch extends AbstractMerkleBranch<String> {

	private static final StringMerkleTree treeInstance = new StringMerkleTree();
	
	public StringMerkleBranch(StringMerkleTree tree, int nodeIndex, List<String> branch) {
		super(tree, nodeIndex, branch);
	}

	public StringMerkleBranch(int nodeIndex, List<String> branch) {
		super(nodeIndex, branch);
	}

	public StringMerkleBranch(int nodeIndex, String node, String root,
			List<String> branch) {
		super(nodeIndex, node, root, branch);
	}

	@Override
	protected String makeParent(String left, String right) {
		return treeInstance.makeParent(left, right);
	}

}
